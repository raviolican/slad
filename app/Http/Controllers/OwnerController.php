<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use \App\Owner;
class OwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function register(Request $request) 
    {    
        $owner = new Owner();
        $owner->username   = $request->username;
        $owner->uuid        = $request->uuid;
        if($owner->exists()) 
        {
            return "owner_exists";
        } else {
            try {
                $owner->save();
            } catch (Exception $ex) {
                echo "error_server";
            }
            echo "success_registration";
        }
    }

    //
}
