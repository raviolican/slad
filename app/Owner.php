<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model {
    protected $fillable = [
        "firstname", "lastname", "uuid"
    ];
    
    public function players() {
        $this->hasMany("App\Players");
    }
    
}